// 1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
//  Функция setTimeout() отрабатывает один раз и прекращает действие, а setInterval() вызывает функцию в указанный интервал времени, но проигрывает ее снова и снова.
// 2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
// Не сработает, так как функция переместится в конец стека и сначала выполнится весь код программы, а потот функция внутри setTimeout.
// 3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
// Потому что важно очистить память, которая была занята выполнением функции setInterval.

let activeImgIndex = 0;
let interval = 0;
const active = 'active';
const imgToShow = document.getElementsByClassName('image-to-show');
const btnStop = document.getElementById('btn-stop');
const btnStart = document.getElementById('btn-start');
const imagesLength = imgToShow.length;

const setActiveImg = () => {
  activeImgIndex++;

  if (activeImgIndex >= imagesLength) {
    activeImgIndex = 0;
  }

  for (let i = 0; i < imagesLength; i++) {
    imgToShow[i].classList.remove(active);
  }

  imgToShow[activeImgIndex].classList.add(active);
};

interval = setInterval(setActiveImg, 10000);

btnStop.addEventListener('click', function () {
  clearInterval(interval);
  interval = 0;
});

btnStart.addEventListener('click', function () {
  if (interval === 0) interval = setInterval(setActiveImg, 10000);
});
